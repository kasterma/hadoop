# Hadoop

Basic experiments with hadoop.

Initial work: getting a virtual cluster set up using vagrant following
http://www.michael-noll.com/tutorials/running-hadoop-on-ubuntu-linux-single-node-cluster/

TODO:

You can also disable IPv6 only for Hadoop as documented in HADOOP-3437. You can do so by adding the following line to conf/hadoop-env.sh:

    export HADOOP_OPTS=-Djava.net.preferIPv4Stack=true