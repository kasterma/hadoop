package { "openjdk-6-jdk":
  ensure => "installed",
}

group { "hadoop":
  ensure => "present",
}

user { "hduser":
  ensure => "present",
  home => "/home/hduser",
  managehome => "true",
  gid => "hadoop",
}