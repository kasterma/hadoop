sudo apt-get update
sudo apt-get install -y puppet
sudo puppet apply /vagrant/vagrant/resources/hadoop.pp
sudo -u hduser ssh-keygen -f "/home/hduser/.ssh/id_rsa" -t rsa -P ""
sudo -u hduser cp /home/hduser/.ssh/id_rsa.pub /home/hduser/.ssh/authorized_keys

# getting java7 from oracle (as recommended in some sources for hadoop)
# instructions are from http://superuser.com/questions/353983/how-do-i-install-the-sun-java-sdk-in-ubuntu-11-10-oneric-and-later-versions
# these work when typed into a terminal, but the oracle java installer requires
# user interaction that I don't know how to do in this script yet.
# sudo apt-get install -y python-software-properties
# sudo add-apt-repository ppa:webupd8team/java
# sudo apt-get update
# sudo apt-get install oracle-java7-installer
